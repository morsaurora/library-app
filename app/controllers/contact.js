import { match, not, and, gte } from '@ember/object/computed';
import Controller from '@ember/controller';

export default Controller.extend({

    emailAddress: '',
    message: '',

    isEmailValid: match('emailAddress', /^.+@.+\..+$/),
    isLongEnough: gte('message.length', 5),
    isValid: and('isEmailValid', 'isLongEnough'),
    isDisabled: not('isValid'),

    actions: {

        contactLibrary() {

            const email = this.get('emailAddress');
            const message = this.get('message');

            const sendMessage = this.store.createRecord('contact', { email: email, message: message })

            sendMessage.save().then(res => {
                this.set('successMessage', `We've received your message with id: ${res.get('id')}. We'll get in touch soon!!`);
                this.set('emailAddress', '');
                this.set('message', '');
            })
        }

    }

});
